﻿using LCDirectoryWebAPI.Context.Models;
using LCDirectoryWebAPI.Data.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using LCDirectoryWebAPI.Data;

namespace LegacyCustomerDirectoryWebAPI
{
    public class EFCService
    {
        public IEnumerable<OldCustomer> GetAllLegacyCustomers()
        {
            using (var context = new AdventureWorks2017Context())
            {
                var query = from person in context.Person


                            join employee in context.Employee on person.BusinessEntityId equals employee.BusinessEntityId

                            join addID in context.BusinessEntityAddress on person.BusinessEntityId equals addID.BusinessEntityId

                            join addtype in context.AddressType on addID.AddressTypeId equals addtype.AddressTypeId

                            join address in context.Address on addID.AddressTypeId equals address.AddressId

                            join phone in context.PersonPhone on person.BusinessEntityId equals phone.BusinessEntityId


                            join phonetype in context.PhoneNumberType on phone.PhoneNumberTypeId equals phonetype.PhoneNumberTypeId

                            join email in context.EmailAddress on person.BusinessEntityId equals email.BusinessEntityId


                            select new OldCustomer()
                            {
                                BusinessEntity = person.BusinessEntityId,
                                FirstName = person.FirstName,
                                LastName = person.LastName,
                                Gender = employee.Gender,
                                JobTitle = employee.JobTitle,
                                MaritalStatus = employee.MaritalStatus,
                                BirthDate = employee.BirthDate,
                                //EmailAddress = null,
                                EmailAddress = email.EmailAddress1 == null ? null : email.EmailAddress1,
                                //EmailAddress = person.EmailAddress.FirstOrDefault() == null ? null : person.EmailAddress.FirstOrDefault().EmailAddress1,
                               //  το πεδίο emailaddress θα πάρει την τιμή null εάν person.Em.FoD == null , αλλιώς θα πάρει το emailaddress1
                                HomePhoneNumber = phonetype.Name == "Home" ? phone.PhoneNumber : null,
                                WorkPhoneNumber = phonetype.Name == "Work" ? phone.PhoneNumber : null,
                                AddressLine1 = address.AddressLine1,
                                AddressLine2 = address.AddressLine2,
                                City = address.City,
                                PostalCode = address.PostalCode


                            };


                            

                            return query.ToList();
                            
                            
            }
        }
    }
}


