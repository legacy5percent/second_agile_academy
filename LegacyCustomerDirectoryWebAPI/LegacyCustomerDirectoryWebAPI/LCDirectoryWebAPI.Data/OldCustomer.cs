﻿using System;

namespace LCDirectoryWebAPI.Data
{
    public class OldCustomer
    {
        public int BusinessEntity { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string JobTitle { get; set; }

        public string MaritalStatus { get; set; }

        public DateTime BirthDate { get; set; }

        public string EmailAddress { get; set; }

        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}
