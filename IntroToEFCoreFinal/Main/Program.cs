﻿using IntroToEFCoreFinal.ADO;
using IntroToEFCoreFinal.Domain;
using IntroToEFCoreFinal.EFCDomain;
using System;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {

            IQueries service1 = new EFCServices();
            Console.WriteLine("Press 1 or 2 for Entity Framework Implementations. Press 3 or 4 for ADO.NET Implementations. Have Fun! \n ");
            int selection = int.Parse(Console.ReadLine());


            if (selection == 1)
            {
                var test = service1.Query1();

                foreach (var x in test)
                {
                    Console.WriteLine("{0}    {1}", x.FirstName, x.LastName);
                    
                }
            }


            else if (selection == 2)
            {
                var test = service1.Query2();

                foreach (var x in test)
                {
                    Console.WriteLine("{0}  {1}   {2}", x.SalesOrderId , x.RevisionNumber, x.OrderDate );
                    
                }
            }


            else if (selection == 3)
            {
                ADOClass classic = new ADOClass();
                classic.Exec1();
                
            }

            else if (selection == 4)
            {
                ADOClass classic = new ADOClass();
                classic.Exec2();
                
            }

            else
            {
                Environment.Exit(0);
            }

            Console.ReadKey();
            Main(args);







        }
    }
}
