﻿using IntroToEFCoreFinal.Domain;
using IntroToEFCoreFinal.Domain.Models;
using IntroToEFCoreFinal.EFCDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IntroToEFCoreFinal.EFCDomain
{
    public class EFCServices : IQueries
    {
        public IEnumerable<Person> Query1()
        {
            using (var context = new AdventureWorks2017Context())
            {
                var query = from b in context.Person
                            join e in context.EmailAddress on b.BusinessEntityId equals e.BusinessEntityId
                            where b.PersonType == "EM" && b.LastName.StartsWith("L")
                            orderby b.FirstName
                            select b;

                return query.ToList();
            }
        }

        public IEnumerable<SalesOrderHeader> Query2()
        {
            using (var context = new AdventureWorks2017Context())
            {
                var query = from c in context.SalesOrderHeader
                            join ert in context.SalesOrderHeaderSalesReason on c.SalesOrderId equals ert.SalesOrderId
                            join mvp in context.SalesReason on ert.SalesReasonId equals mvp.SalesReasonId
                            where mvp.ReasonType.Contains("Marketing")
                            select c;
     
                return query.ToList();
            }
        }
    }

    
}
