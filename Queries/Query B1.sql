SELECT [SalesTerritory].[Name] AS 'Territory Name' , 

ROUND(MIN([SalesOrderHeader].[TotalDue]),2) AS 'Min Total Due' , 
CEILING( MAX([SalesOrderHeader].[TotalDue])) AS 'Max Total Due Rdup' 

FROM Sales.SalesOrderHeader

INNER JOIN Sales.SalesTerritory ON SalesOrderHeader.TerritoryID = SalesTerritory.TerritoryID

GROUP BY   SalesTerritory.Name
ORDER BY   SalesTerritory.Name


    

	