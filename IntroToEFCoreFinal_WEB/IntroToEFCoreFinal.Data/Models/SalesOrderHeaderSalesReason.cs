﻿using System;
using System.Collections.Generic;

namespace IntroToEFCoreFinal.EFCDomain.Models
{
    public partial class SalesOrderHeaderSalesReason
    {
        public int SalesOrderId { get; set; }
        public int SalesReasonId { get; set; }
        public DateTime ModifiedDate { get; set; }

        public virtual SalesOrderHeader SalesOrder { get; set; }
    }
}
