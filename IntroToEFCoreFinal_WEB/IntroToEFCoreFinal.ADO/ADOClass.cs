﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using System.Data;

namespace IntroToEFCoreFinal.ADO
{
    public class ADOClass
    {

        public void Exec1()
        {


            string connectionString =
            " Server = DESKTOP-1N4MEBG;Database=AdventureWorks2017;Trusted_Connection=True; "
            + " Integrated Security=true ";

            // Provide the query string with a parameter placeholder.
            string queryString =
                " SELECT FirstName, LastName, EmailAddress FROM Person.Person JOIN Person.EmailAddress on Person.BusinessEntityID = EmailAddress.BusinessEntityID  WHERE Person.Person.LastName >= 'L' AND Person.Person.LastName < 'M' AND Person.PersonType = 'EM' ; ";





            // Specify the parameter value.
            int paramValue = 5;

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {

                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@pricePoint", paramValue);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}",
                            reader[0], reader[1]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }


        public void Exec2() {
            string connectionString =
                " Server = DESKTOP-1N4MEBG;Database=AdventureWorks2017;Trusted_Connection=True; "
                + " Integrated Security=true ";

            // Provide the query string with a parameter placeholder.
            string queryString =
                " SELECT * FROM [AdventureWorks2017].[Sales].SalesOrderHeader INNER JOIN[AdventureWorks2017].[Sales].[SalesOrderHeaderSalesReason] ON SalesOrderHeader.[SalesOrderID] = SalesOrderHeaderSalesReason.SalesOrderID WHERE[SalesOrderHeaderSalesReason].[SalesReasonID] IN(SELECT[SalesReasonID] FROM [AdventureWorks2017].[Sales].SalesReason WHERE ReasonType like 'Marketing' )";





            // Specify the parameter value.
            int paramValue = 5;

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {

                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                command.Parameters.AddWithValue("@pricePoint", paramValue);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}",
                            reader[0], reader[1], reader[2], reader[3]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }

    }




}

        
    
