﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class Address
    {
        public Address()
        {
            PersonAddressCon = new HashSet<PersonAddressCon>();
        }

        public Guid AddressId { get; set; }
        public string Street { get; set; }
        public string HouseNr { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public virtual ICollection<PersonAddressCon> PersonAddressCon { get; set; }
    }
}
