﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderProducts = new HashSet<OrderProducts>();
        }

        public Guid OrderId { get; set; }
        public Guid PersonId { get; set; }
        public DateTime DateTime { get; set; }

        public virtual Person Person { get; set; }
        public virtual ICollection<OrderProducts> OrderProducts { get; set; }
    }
}
