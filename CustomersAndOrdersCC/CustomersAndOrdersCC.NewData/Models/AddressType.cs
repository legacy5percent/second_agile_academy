﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class AddressType
    {
        public AddressType()
        {
            PersonAddressCon = new HashSet<PersonAddressCon>();
        }

        public int AddressTypeId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<PersonAddressCon> PersonAddressCon { get; set; }
    }
}
