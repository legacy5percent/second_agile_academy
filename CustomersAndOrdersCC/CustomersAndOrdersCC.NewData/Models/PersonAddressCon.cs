﻿using System;
using System.Collections.Generic;

namespace CustomersAndOrdersCC.NewData.Models
{
    public partial class PersonAddressCon
    {
        public Guid PersonId { get; set; }
        public Guid AddressId { get; set; }
        public int AddressTypeId { get; set; }

        public virtual Address Address { get; set; }
        public virtual AddressType AddressType { get; set; }
        public virtual Person Person { get; set; }
    }
}
