﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class Address
    {
        public Address()
        {
            CustomerAddressCon = new HashSet<CustomerAddressCon>();
        }

        public int AddressId { get; set; }
        public string Street { get; set; }
        public string HouseNr { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public virtual ICollection<CustomerAddressCon> CustomerAddressCon { get; set; }
    }
}
