﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderProducts = new HashSet<OrderProducts>();
        }

        public int ProductId { get; set; }
        public int ProductTypeId { get; set; }
        public string Description { get; set; }
        public int UnitTypeId { get; set; }
        public decimal Price { get; set; }

        public virtual ProductType ProductType { get; set; }
        public virtual UnitType UnitType { get; set; }
        public virtual ICollection<OrderProducts> OrderProducts { get; set; }
    }
}
