﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class Customer
    {
        public Customer()
        {
            CustomerAddressCon = new HashSet<CustomerAddressCon>();
            Order = new HashSet<Order>();
        }

        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PhoneNr { get; set; }
        public string EmailAddress { get; set; }
        public string Gender { get; set; }

        public virtual ICollection<CustomerAddressCon> CustomerAddressCon { get; set; }
        public virtual ICollection<Order> Order { get; set; }
    }
}
