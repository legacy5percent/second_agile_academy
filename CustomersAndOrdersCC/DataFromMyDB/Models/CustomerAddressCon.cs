﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class CustomerAddressCon
    {
        public int AddressId { get; set; }
        public int CustomerId { get; set; }
        public int AddressTypeId { get; set; }

        public virtual Address Address { get; set; }
        public virtual AddressType AddressType { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
