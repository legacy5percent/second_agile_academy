﻿using System;
using System.Collections.Generic;

namespace DataFromMyDB.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderProducts = new HashSet<OrderProducts>();
        }

        public int OrderId { get; set; }
        public int CustomerId { get; set; }
        public DateTime DateTime { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual ICollection<OrderProducts> OrderProducts { get; set; }
    }
}
