﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CustomersAndOrdersCC.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class OcOverviewController : Controller
    {
        [HttpPost]
        public async Task<ActionResult> CustomerOverview(ModelClass model)
        {
            //I have already defined this globally as static :
            List<OldCustomer> PersInfo = new List<OldCustomer>();
            string Baseurl = "http://localhost:5000/";

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/values");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var PersonResponse = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    PersInfo = JsonConvert.DeserializeObject<List<OldCustomer>>(PersonResponse);

                }
                HttpContext.Session.SetObjectAsJson("_oldCustomer", PersInfo);
                return View("CustomerOverview", PersInfo);
            }

        }

    }
    }