﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomersAndOrdersCC.Data.Models;
using CustomersAndOrdersCC.NewData.Models;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class CreateCustomerController : Controller
    {
        [HttpPost]
        public IActionResult CreateCustomer(Customer PersInfo)
        {
            CombinedModel Combo = new CombinedModel();

            PersInfo.PersonID = Guid.NewGuid();
            Guid AddressId = Guid.NewGuid();
            

            using (var db = new MyNewDatabaseContext())
            {
                var personDB = db.Set<Person>();
                var addressDB = db.Set<Address>();
                var personaddconDB = db.Set<PersonAddressCon>();

                personDB.Add(new Person
                {
                  BusinessEntityId = PersInfo.BusinessEntityID,
                  PersonId = PersInfo.PersonID,
                  FirstName = PersInfo.FirstName,
                  LastName = PersInfo.LastName,
                  MiddleName = PersInfo.MiddleName,
                  Title = PersInfo.Title,
                  PhoneNr = PersInfo.HomePhoneNumber,
                  EmailAddress = PersInfo.EmailAddress,
                  Gender = PersInfo.Gender
                });

                addressDB.Add(new Address
                {
                    AddressId = AddressId,
                    Street = PersInfo.AddressLine1,
                    HouseNr = PersInfo.AddressLine2,
                    City = PersInfo.City,
                    PostalCode = PersInfo.PostalCode

                });

                personaddconDB.Add(new PersonAddressCon
                {
                    PersonId = PersInfo.PersonID,
                    AddressId = AddressId,
                    AddressTypeId = 1
                });
                


            db.SaveChanges();
            }

            var efcservice = new NewEFCService();
            Combo = efcservice.FetchProducts(PersInfo);
            HttpContext.Session.SetObjectAsJson("_Combo", Combo);
            return View("Order",Combo);
        }
    }
}