﻿using CustomersAndOrdersCC.Data.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication
{
    public static class CommonFunctions
    {

        public static void SetObjectAsJson(this ISession session, string key, object value)
        {

            string prefix = "{'Person':";
            string suffix = "}";
            string s = JsonConvert.SerializeObject(value);
            session.SetString(key, s);
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }


        public static decimal calculateTotalPrice(List<OrderingProduct> Oproduct)


        {
            decimal totalprice = 0;
            foreach (var item in Oproduct)
            {

                totalprice = totalprice + (item.Price * item.Quantity);
            }
            return totalprice;
        }

        /*
        public static NewCustomer TransferInfo(Customer old)
        {
            NewCustomer transferNew = new NewCustomer();

            transferNew.BusinessEntityID = old.BusinessEntityID;
            transferNew.FirstName = old.FirstName;
            transferNew.LastName = old.LastName;
            transferNew.Gender = old.Gender;
            transferNew.JobTitle = old.JobTitle;
            transferNew.MaritalStatus = old.MaritalStatus;
            transferNew.BirthDate = old.BirthDate;
            transferNew.EmailAddress = old.EmailAddress;
            transferNew.HomePhoneNumber = old.HomePhoneNumber;
            transferNew.WorkPhoneNumber = old.WorkPhoneNumber;
            transferNew.AddressLine1 = old.AddressLine1;
            transferNew.AddressLine2 = old.AddressLine2;
            transferNew.City = old.City;
            transferNew.PostalCode = old.PostalCode;

            return transferNew;

             
        }
        */
    }




}
