﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersAndOrdersCC.Data.Models
{
    public class NewCustomer : Customer
    {
        public string HomeAddress { get; set; }
        public DateTime DateAdded { get; set; }
        public string EmailAddressNew { get; set; }
    }
}
