﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersAndOrdersCC.Data.Models
{
    public class OrderingProduct
    {
        public Guid ProductId { get; set; }
        public string ProductType { get; set; }
        public string Description { get; set; }
        public string UnitType { get; set; }
        public decimal Price { get; set; }

        public Boolean IsChecked { get; set; }

        public int Quantity { get; set; }
    }
}
