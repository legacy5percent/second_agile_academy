﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersAndOrdersCC.Data.Models
{
    public class Customer
    {
        public int? BusinessEntityID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Title { get; set; }

        public string Gender { get; set; }

        public string JobTitle { get; set; }

        public string MaritalStatus { get; set; }

        public DateTime BirthDate { get; set; }

        public string EmailAddress { get; set; }

        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }

        public Guid PersonID { get; set; }

        public bool? OldDatabase { get; set; }

        
    }
}
